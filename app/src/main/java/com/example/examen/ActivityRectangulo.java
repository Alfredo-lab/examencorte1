package com.example.examen;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class ActivityRectangulo extends AppCompatActivity {
    rectangulo rectangulo;
    private TextView lblNombre;
    private EditText txtBase;
    private EditText txtAltura;

    private TextView lblArea;
    private TextView lblPerimetro;

    private Button btnCalcular;
    private Button btnRegresar;
    private Button btnLimpiar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rectangulo);
        lblNombre = (TextView) findViewById(R.id.txtNombreR);
        lblArea = (TextView) findViewById(R.id.lblArea);
        lblPerimetro = (TextView) findViewById(R.id.lblPerimetro);

        txtBase = (EditText) findViewById(R.id.txtBase);
        txtAltura = (EditText) findViewById(R.id.txtAltura);

        btnCalcular = (Button) findViewById(R.id.btnCalcular);
        btnRegresar = (Button) findViewById(R.id.btnRegresar);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);

        Bundle datos = getIntent().getExtras();

        lblNombre.setText("Mi nombre es: " + (datos.getString("Nombre")));

        rectangulo  = (rectangulo) datos.getSerializable("rectangulo");
        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (txtBase.getText().toString().matches("") || txtAltura.getText().toString().matches("")){
                    Toast.makeText(ActivityRectangulo.this, "Campos Vacios",Toast.LENGTH_SHORT).show();
                }
                else{
                    rectangulo.setBase(Integer.valueOf(txtBase.getText().toString()));
                    rectangulo.setAltura(Integer.valueOf(txtAltura.getText().toString()));

                    lblArea.setText(String.valueOf(rectangulo.calcularArea()));
                    lblPerimetro.setText(String.valueOf(rectangulo.calcularPerimetro()));
                }
            }
        });
        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txtBase.setText("");
                txtAltura.setText("");
                lblArea.setText("");
                lblPerimetro.setText("");
            }
        });
        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
}
