package com.example.examen;

import java.io.Serializable;

public class rectangulo implements Serializable {
    private int base;
    private int altura;

    public rectangulo(int base, int altura) {
        this.setBase(base);
        this.setAltura(altura);
    }

    public rectangulo() {

    }

    public int getBase() {
        return base;
    }

    public void setBase(int base) {
        this.base = base;
    }

    public int getAltura() {
        return altura;
    }

    public void setAltura(int altura) {
        this.altura = altura;
    }
    public int calcularArea(){
        return (this.base * this.altura);
    }
    public int calcularPerimetro(){
        return (2 * (this.base + this.altura));
    }
}
